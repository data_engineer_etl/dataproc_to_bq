#!/bin/bash
# Initialisation du cluster Dataproc
echo "Initialization action started successfully."

# Définir les variables d'environnement
export GCP_PROJECT="enduring-tea-423217-k5"
export BQ_DATASET="dataproc_dataset"
export BQ_TABLE="dataproc_table"

# Exécuter le job PySpark
gcloud dataproc jobs submit pyspark gs://dataproc_bucket/dataproc_job.py \
    --cluster=dataproc-cluster \
    --region=us-central1

echo "Initialization action executed successfully."
