project      = "enduring-tea-423217-k5"
region       = "us-central1"
bucket_name  = "dataproc_buck"
cluster_name = "dataproc-cluster"
dataset_id   = "dataproc_dataset"
table_id     = "dataproc_table"
schema_path  = "schema.json"
