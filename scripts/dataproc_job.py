from pyspark.sql import SparkSession
from pyspark.sql.functions import col
import os

def main():
    spark = SparkSession.builder \
        .appName('Dataproc to BigQuery') \
        .getOrCreate()

    # Lire les données de GCS
    df = spark.read.json('gs://dataproc_bucket/data.json')

    # Transformation 1: Filtrer les lignes où l'âge est supérieur à 10
    df_filtered = df.filter(col("age") < 10)

    # Transformation 2: Ajouter une colonne "full_name" en combinant "first_name" et "last_name"
    df_transformed = df_filtered.withColumn("full_name", col("first_name") + " " + col("last_name"))

    # Transformation 3: Sélectionner les colonnes "full_name" et "age"
    df_final = df_transformed.select("full_name", "age")

    # Utiliser les variables d'environnement pour BigQuery
    project_id = os.getenv('GCP_PROJECT')
    dataset_id = os.getenv('BQ_DATASET')
    table_id = os.getenv('BQ_TABLE')

    # Écrire les données dans BigQuery
    df_final.write.format('bigquery') \
        .option('table', f'{project_id}.{dataset_id}.{table_id}') \
        .save()

    spark.stop()

if __name__ == '__main__':
    main()
