provider "google" {
  project = var.project
  region  = var.region
}

module "service_account" {
  source  = "./modules/service_account"
  project = var.project
}

module "gcs" {
  source      = "./modules/gcs"
  project     = var.project
  bucket_name = var.bucket_name
  region      = var.region
}

module "dataproc" {
  source                = "./modules/dataproc"
  project               = var.project
  region                = var.region
  cluster_name          = var.cluster_name
  bucket_name           = module.gcs.bucket_name
  service_account_email = module.service_account.email
}

module "bigquery" {
  source      = "./modules/bigquery"
  project     = var.project
  dataset_id  = var.dataset_id
  table_id    = var.table_id
  schema_path = var.schema_path
}
