variable "project" {
  description = "The GCP project ID"
  type        = string
}

variable "region" {
  description = "The GCP region"
  type        = string
}

variable "bucket_name" {
  description = "The name of the GCS bucket"
  type        = string
}

variable "cluster_name" {
  description = "The name of the Dataproc cluster"
  type        = string
}

variable "dataset_id" {
  description = "The BigQuery dataset ID"
  type        = string
}

variable "table_id" {
  description = "The BigQuery table ID"
  type        = string
}

variable "schema_path" {
  description = "Path to the BigQuery table schema"
  type        = string
}
