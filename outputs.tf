output "bucket_name" {
  value = module.gcs.bucket_name
}

output "dataproc_cluster_name" {
  value = module.dataproc.cluster_name
}

output "bigquery_table_id" {
  value = module.bigquery.table_id
}
