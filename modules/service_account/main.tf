resource "google_service_account" "dataproc_sa" {
  project      = var.project
  account_id   = "dataproc-sa"
  display_name = "Dataproc to BigQuery Service Account"
}

resource "google_project_iam_member" "dataproc_sa_roles" {
  for_each = toset([
    "roles/dataproc.editor",
    "roles/dataproc.worker",
    "roles/storage.objectAdmin",
    "roles/bigquery.dataEditor",
    "roles/bigquery.jobUser"
  ])

  project = var.project
  role    = each.key
  member  = "serviceAccount:${google_service_account.dataproc_sa.email}"
}
