resource "google_bigquery_dataset" "dataset" {
    project = var.project
    dataset_id = var.dataset_id
    location   = "us-central1"
}

resource "google_bigquery_table" "table" {
    project = var.project
    dataset_id = google_bigquery_dataset.dataset.dataset_id
    table_id   = var.table_id

    schema     = file("${path.module}/../../schema.json")
    deletion_protection = false

}
