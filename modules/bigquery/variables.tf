variable "project" {
  description = "The ID of the GCP project"
  type        = string
}

variable "dataset_id" {
  description = "The ID of the dataset"
  type        = string  
}

variable "table_id" {
  description = "The ID of the table"
  type        = string
}

variable "schema_path" {
  description = "The path to the schema file"
  type        = string
  
}