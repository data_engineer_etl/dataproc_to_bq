resource "google_storage_bucket" "dataproc_buck" {
  name          = var.bucket_name
  location      = var.region
  force_destroy = true
}


