variable "project" {
  description = "The ID of the GCP project"
  type        = string
}

variable "bucket_name" {
  description = "The name of the bucket"
  type        = string
}
variable "region" {
  description = "The location for the bucket"
  type        = string
}
