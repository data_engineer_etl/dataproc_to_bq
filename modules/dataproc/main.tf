resource "google_dataproc_cluster" "cluster" {
  name       = var.cluster_name
  region     = var.region
  project    = var.project

  cluster_config {
    staging_bucket = var.bucket_name

    gce_cluster_config {
      service_account = var.service_account_email
    }

    master_config {
      num_instances = 1
      machine_type  = "n1-standard-2"  # Type de machine corrigé
    }

    worker_config {
      num_instances = 2
      machine_type  = "n1-standard-2"  # Type de machine corrigé
    }

    initialization_action {
      script = "gs://${var.bucket_name}/init-actions.sh"
    }
  }
}

